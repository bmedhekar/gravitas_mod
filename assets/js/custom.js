var FIproject = {};

var winWidth = $(window).width();

var vidPath = "assets/videos/home-banner.mp4";

var dynString = '<video autoplay loop muted src="' + vidPath + '" width="100%" height="100%" type="video/mp4"></video>';

function homeBannerSlider() {
    var homeBanner = new Swiper(".banner-slider", {
        pagination: {
            el: ".swiper-pagination"
        },
        slidesPerView: 1,
        autoplay: {
            delay: 8e3
        },
        speed: 600,
        loop: false,
        on: {
            slideChange: function () {
                var currentSlide = homeBanner.activeIndex;
                if (currentSlide == 2) {
                    setTimeout(function () {
                        mySwiper.slideTo(1);
                    }, 8e3);
                }
            }
        }
    });
}

function slideActiveTab() {
    var tabItemLeftPos = $(".cp-tabs .tab-item.active").position().left;
    var tabItemWidth = $(".cp-tabs .tab-item.active").innerWidth();
    $(".slide-me").css({
        left: tabItemLeftPos,
        width: tabItemWidth
    });
}

function customTab() {
    $(".cp-tabs .tab-item").click(function () {
        var clickedTab = $(this).attr("data-id");
        if (!$(this).hasClass("active")) {
            $(".cp-tabs .tab-item").removeClass("active");
            $(this).addClass("active");
            $(".cp-tabs #" + clickedTab).siblings().hide();
            $(".cp-tabs #" + clickedTab).fadeIn(400);
            slideActiveTab();
        }
    });
}

function setBg() {
    $(".set-bg").each(function () {
        var imgPath = $(this).find("img").attr("src");
        $(this).css("background-image", "url(" + imgPath + ")");
    });
}

function discoverSlider() {
    $(".cp-discover .discover-item").click(function () {
        $(".cp-discover .discover-item").removeClass("selected");
        $(this).addClass("selected");
        var imgWrap = $(this).closest(".cp-section").find(".sec-bg-wrap .img-wrap");
        var activeSlide = $(this).attr("data-count");
        $(imgWrap).removeClass("active");
        $("." + activeSlide).addClass("active");
    });
}

var headerTop = $("header").offset().top;

function headerSticky() {
    var scroll = $(window).scrollTop();
    console.log(headerTop);
    if (scroll >= 100) {
        $(".cp-header").addClass("typ-white");
    } else {
        $(".cp-header").removeClass("typ-white");
        $(".cp-section .scroll-btn").fadeIn(400);
    }
}

function menuOpen() {
    var colLnt = $(".menu-col").length;
    $(".menu-btn").click(function () {
        $(".cp-menu").addClass("active");
        $(".cp-header").css("opacity", 0);
        $(".cp-menu .menu-head").addClass("show");
        var count = 0;
        for (var i = 0; i < colLnt; i++) {
            delayFunc(i, count);
            count += 200;
        }
        function delayFunc(a, ct) {
            setTimeout(function () {
                $(".menu-col").eq(a).addClass("down");
                setTimeout(function () {
                    $(".menu-col").eq(a).find(".nav-wrap").addClass("reveallinks");
                }, a * 200);
            }, ct);
        }
        setTimeout(function () {
            $(".cp-menu").addClass("hover-effect");
        }, 1200);
    });
    $(".close-btn").click(function () {
        $(".cp-menu").removeClass("hover-effect");
        $(".menu-col").removeClass("hover-bg");
        $(".menu-col").css("width", 25 + "%");
        $(".menu-col").find(".nav-wrap").removeClass("hidelinks");
        var count = 200;
        for (var i = colLnt - 1; i >= 0; i--) {
            delayFunc(i, count);
            count += 200;
        }
        function delayFunc(a, ct) {
            setTimeout(function () {
                $(".menu-col").eq(a).removeClass("down");
            }, ct);
        }
        setTimeout(function () {
            $(".cp-menu .menu-head").removeClass("show");
            $(".cp-header").css("opacity", 1);
            $(".cp-menu").removeClass("active");
            $(".menu-col .nav-wrap").removeClass("reveallinks");
        }, 1e3);
    });
    $(document).on("mouseover", ".cp-menu.hover-effect .menu-col", function () {
        var currTgt = $(this).index();
        $(".menu-col").removeClass("hover-bg");
        $(this).addClass("hover-bg");
        $(this).find(".nav-wrap").removeClass("hidelinks");
        for (var i = 0; i < colLnt; i++) {
            if (currTgt != i) {
                $(".menu-col").eq(i).css("width", 20 + "%");
                $(".menu-col").eq(i).find(".nav-wrap").addClass("hidelinks");
            } else {
                $(this).css("width", 40 + "%");
            }
        }
    });
    $(window).on("mouseleave", function () {
        $(".menu-col").removeClass("hover-bg");
        $(".menu-col").css("width", 25 + "%");
        $(".menu-col").find(".nav-wrap").removeClass("hidelinks");
    });
}

$(function () {
    setTimeout(function () {
        $(".cm-loader .logo").addClass("reveal");
    }, 1400);
    skrollr.init({
        forceHeight: false
    });
    homeBannerSlider();
    discoverSlider();
    setBg();
    menuOpen();
    customTab();
    window.onscroll = function () {
        headerSticky();
    };
    $(".cp-section .scroll-btn").on("click", function () {
        var obj = $(this);
        setTimeout(function() {
            obj.fadeOut(400);
        },1000);
        var currTgt = $(this).closest(".cp-section").offset().top;
        var headerH = $("header").outerHeight();
        $("html,body").animate({
            scrollTop: currTgt - headerH
        }, 800);
    });
});

$(window).on("load", function () {
    slideActiveTab();
    setTimeout(function () {
        $(".cm-loader").removeClass("active");
        $(".cm-loader .logo").css("opacity", 0);
    }, 1e3);
    setTimeout(function () {
        $(".cm-loader").hide();
    }, 1500);
    $(".video-wrap").append(dynString);
});